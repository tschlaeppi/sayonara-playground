#!/bin/bash

set -e

LOG_STAGE_PREFIX="BUILD: BASE:"
SAYONARA_BUILDDIR="./build"

echo "$LOG_STAGE_PREFIX Starting"

if [[ -d "$SAYONARA_BUILDDIR" ]]
then
    echo "$LOG_STAGE_PREFIX SAYONARA_BUILDDIR exists - removing"
    rm -rf $SAYONARA_BUILDDIR
fi

echo "$LOG_STAGE_PREFIX Creating SAYONARA_BUILDDIR"
mkdir $SAYONARA_BUILDDIR
cd $SAYONARA_BUILDDIR

echo "$LOG_STAGE_PREFIX Running cmake"
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_TESTS=ON -DWITH_DOC=ON -GNinja ..

echo "$LOG_STAGE_PREFIX Building Sayonara Player"
ninja

echo "$LOG_STAGE_PREFIX Testing Sayonara Player"
export QT_QPA_PLATFORM='offscreen'
./test/EditorTest

ninja test

echo "$LOG_STAGE_PREFIX Creating Sayonara Player Package (tar.gz)"
ninja package

echo "$LOG_STAGE_PREFIX Finished"
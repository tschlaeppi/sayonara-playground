Name:           sayonara
Version:        @SAYONARA_VERSION_BASE@
Release:        @SAYONARA_VERSION_RELEASE@
Summary:        A lightweight Qt Audio player

License:        GPLv3+
URL:            https://sayonara-player.com

Source0:        sayonara-player-@SAYONARA_VERSION_BASE@-@SAYONARA_VERSION_RELEASE@.tar.gz

BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  gstreamer1-plugins-base-devel
BuildRequires:  zlib-devel
Requires:		qt5-qtsvg
Requires:		hicolor-icon-theme
Requires:		gstreamer1-plugins-bad-free

%description
%{name} is a small, clear, not yet platform-independent music player. Low 
CPU usage, low memory consumption and no long loading times are only three 
benefits of this player. Sayonara should be easy and intuitive to use and 
therefore it should be able to compete with the most popular music players.

%prep
%setup -q -n %{name}-player
rm -rf .git*

%build
%cmake . -DCMAKE_BUILD_TYPE="RelWithDebInfo" -DCMAKE_INSTALL_PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

# remove menu dir, because it's not necessary
rm -rf %{buildroot}/%{_datadir}/menu

find %{buildroot}%{_datadir}/%{name}/translations -name "*.qm" | sed 's:'%{buildroot}'::
s:.*/\([a-zA-Z]\{2\}\).qm:%lang(\1) \0:' > %{name}.lang

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/appdata/*.appdata.xml

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
/usr/bin/update-desktop-database &> /dev/null || :

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%license LICENSE
%doc MANUAL README.md
%doc %{_datadir}/man/man1/%{name}.1.gz
%{_bindir}/%{name}
%{_bindir}/%{name}-query
%{_bindir}/%{name}-ctl
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.xpm
%{_datadir}/appdata/%{name}.appdata.xml
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/translations
%dir %{_datadir}/%{name}/translations/icons
%{_datadir}/%{name}/translations/icons/*.png

%changelog
* @SAYONARA_DATE_TODAY@ Michael Lugmair <sayonara-player@posteo.org> - @SAYONARA_VERSION@
- New Release @SAYONARA_VERSION@

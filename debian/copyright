Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sayonara Player
Upstream-Contact: sayonara-player@posteo.org
Source: http://sayonara-player.com/
License: GPL-3+

Files: *
Copyright: 2011-2019 Lucio Carreras <sayonara-player@posteo.org>
License: GPL-3+

Files: src/3rdParty/taglib
Copyright: 2002-2008 Scott Wheeler <wheeler@kde.org>
License: LGPL-2.1
Source: https://taglib.org/

Files: src/Gui/Resources/MintYIcons
Copyright: 2010-2014 Clement Lefebvre <root@linuxmint.com>
License: GPL-3+ and CC-BY-SA-4
Source: https://github.com/linuxmint/mint-y-icons


License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 . 
 On Debian systems, the full text of the GNU General Public 
 License version 3 can be found in the file
 '/usr/share/common-licenses/GPL-3'.


License: LGPL-2.1
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License, version 2.1, as
  published by the Free Software Foundation.
  .
  This file is provided "AS IS", without WARRANTIES OR CONDITIONS OF ANY
  KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY
  WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR
  FITNESS FOR A PARTICULAR PURPOSE.
  .
  On Debian systems, the full text of the GNU Lesser General Public
  License version 2.1 can be found in the file
  `/usr/share/common-licenses/LGPL-2.1'.
